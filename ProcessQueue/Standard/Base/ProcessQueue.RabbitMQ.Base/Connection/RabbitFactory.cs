﻿using ProcessQueue.RabbitMQ.Base.Model;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProcessQueue.RabbitMQ.Base.Connection
{
    /// <summary>
    /// Fabryka połączenia do serwisu RabbitMQ
    /// </summary>
    public class RabbitFactory
    {
        /// <summary>
        /// Metoda zwraca fabrykę połączenia do serwisu RabbitMQ
        /// </summary>
        /// <param name="rabbitConnectionModel">Klasa modelu danych połączenia</param>
        /// <returns></returns>
        public static ConnectionFactory GetConnectionFactory(IRabbitConnectionModel rabbitConnectionModel)
        {
            var connectionFactory = new ConnectionFactory();

            connectionFactory.UserName = rabbitConnectionModel.UserName;
            connectionFactory.Password = rabbitConnectionModel.Password;
            connectionFactory.VirtualHost = rabbitConnectionModel.VirtualHost;
            connectionFactory.HostName = rabbitConnectionModel.HostName;
            connectionFactory.Port = rabbitConnectionModel.Port;

            return connectionFactory;
        }

        /// <summary>
        /// Metoda zwraca połączenie do serwisu RabbitMQ
        /// </summary>
        /// <param name="connectionFactory">Fabryka połączeń</param>
        /// <returns></returns>
        public static IConnection GetConnection(IConnectionFactory connectionFactory)
        {
            return connectionFactory.CreateConnection();
        }

        /// <summary>
        /// Metoda zwraca połączenie/model do serwisu RabbitMQ
        /// </summary>
        /// <param name="connection">Klasa połączenia do serwisu</param>
        /// <returns></returns>
        public static IModel GetChannel(IConnection connection)
        {
            return connection.CreateModel();
        }
    }
}
