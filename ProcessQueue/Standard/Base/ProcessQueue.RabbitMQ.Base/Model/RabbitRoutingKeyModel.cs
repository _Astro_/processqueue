﻿using ProcessQueue.RabbitMQ.Base.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace ProcessQueue.RabbitMQ.Base.Model
{
    public class RabbitRoutingKeyModel :ConfigurationElement,  IRabbitRoutingKeyModel
    {
        [ConfigurationProperty("Name")]
        public string Name
        {
            get
            {
                return (string)this[ApplicationStrings.ConfigurationRabbitRoutingKeyModelNamePropertyName];
            }
            set
            {
                this[ApplicationStrings.ConfigurationRabbitRoutingKeyModelNamePropertyName] = value;
            }
        }
    }
}
