﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace ProcessQueue.RabbitMQ.Base.Model
{
    [ConfigurationCollection(typeof(RabbitExchangeModel), AddItemName = "RabbitExchange")]

    public class RabbitExchangeCollection : ConfigurationElementCollection, IEnumerable<RabbitExchangeModel>
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new RabbitExchangeModel();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return (element as RabbitExchangeModel).Name;
        }

        IEnumerator<RabbitExchangeModel> IEnumerable<RabbitExchangeModel>.GetEnumerator()
        {
            foreach (var key in this.BaseGetAllKeys())
            {
                yield return (RabbitExchangeModel)BaseGet(key);
            }
        }
    }
}
