﻿using System.Collections.Generic;

namespace ProcessQueue.RabbitMQ.Base.Model
{
    public interface IRabbitQueueModel
    {
        int Lp { get; set; }
        string Name { get; set; }
        string Exchange { get; set; }
        RabbitRoutingKeyCollection RabbitRoutingKeys { get; set; }
    }
}