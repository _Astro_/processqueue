﻿using ProcessQueue.RabbitMQ.Base.Common;

namespace ProcessQueue.RabbitMQ.Base.Model
{
    public interface IRabbitCoreModel
    {
        ServiceCommunicationType Communication { get; set; }
        bool Durable { get; set; }
    }
}