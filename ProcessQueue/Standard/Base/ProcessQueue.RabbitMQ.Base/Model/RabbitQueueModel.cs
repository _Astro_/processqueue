﻿using ProcessQueue.RabbitMQ.Base.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace ProcessQueue.RabbitMQ.Base.Model
{
    public class RabbitQueueModel : ConfigurationElement, IRabbitQueueModel
    {
        [ConfigurationProperty("Lp")]
        public int Lp
        {
            get
            {
                return (int)this[ApplicationStrings.ConfigurationRabbitQueueModelLpPropertyName];
            }
            set
            {
                this[ApplicationStrings.ConfigurationRabbitQueueModelLpPropertyName] = value;
            }
        }

        [ConfigurationProperty("Name")]
        public string Name
        {
            get
            {
                return (string)this[ApplicationStrings.ConfigurationRabbitQueueModelNamePropertyName];
            }
            set
            { 
                this[ApplicationStrings.ConfigurationRabbitQueueModelNamePropertyName] = value; 
            }
        }

        [ConfigurationProperty("Exchange")]
        public string Exchange
        {
            get
            {
                return (string)this[ApplicationStrings.ConfigurationRabbitQueueModelExchangePropertyName];
            }
            set
            {
                this[ApplicationStrings.ConfigurationRabbitQueueModelExchangePropertyName] = value;
            }
        }

        [ConfigurationProperty("RabbitRoutingKeys", IsDefaultCollection = true)]
        public RabbitRoutingKeyCollection RabbitRoutingKeys
        {
            get
            {
                return (RabbitRoutingKeyCollection)this[ApplicationStrings.ConfigurationRabbitQueueModelRoutingListPropertyName];
            }
            set
            {
                this[ApplicationStrings.ConfigurationRabbitQueueModelRoutingListPropertyName] = value;
            }
        }
    }
}
