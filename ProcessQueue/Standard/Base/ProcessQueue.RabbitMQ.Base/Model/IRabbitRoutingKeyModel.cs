﻿namespace ProcessQueue.RabbitMQ.Base.Model
{
    public interface IRabbitRoutingKeyModel
    {
        string Name { get; set; }
    }
}