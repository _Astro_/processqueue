﻿using ProcessQueue.RabbitMQ.Base.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace ProcessQueue.RabbitMQ.Base.Model
{
    public class RabbitConnectionModel : ConfigurationElement, IRabbitConnectionModel
    {
        [ConfigurationProperty("UserName", IsRequired = true)]
        public string UserName
        {
            get
            {
                return (string)this[ApplicationStrings.ConfigurationRabbitConnectionModelUserName];
            }
            set
            {
                this[ApplicationStrings.ConfigurationRabbitConnectionModelUserName] = value;
            }
        }

        [ConfigurationProperty("Password", IsRequired = true)]
        public string Password
        {
            get
            {
                return (string)this[ApplicationStrings.ConfigurationRabbitConnectionModelUserPassword];
            }
            set
            {
                this[ApplicationStrings.ConfigurationRabbitConnectionModelUserPassword] = value;
            }
        }

        [ConfigurationProperty("VirtualHost", IsRequired = true)]
        public string VirtualHost
        {
            get
            {
                return (string)this[ApplicationStrings.ConfigurationRabbitConnectionModelVirtualHost];
            }
            set
            {
                this[ApplicationStrings.ConfigurationRabbitConnectionModelVirtualHost] = value;
            }
        }

        [ConfigurationProperty("HostName", IsRequired = true)]
        public string HostName
        {
            get
            {
                return (string)this[ApplicationStrings.ConfigurationRabbitConnectionModelHostName];
            }
            set
            {
                this[ApplicationStrings.ConfigurationRabbitConnectionModelHostName] = value;
            }
        }

        [ConfigurationProperty("Port", IsRequired = true)]
        public int Port
        {
            get
            {
                return (int)this[ApplicationStrings.ConfigurationRabbitConnectionModelPort];
            }
            set
            {
                this[ApplicationStrings.ConfigurationRabbitConnectionModelPort] = value;
            }
        }
    }
}
