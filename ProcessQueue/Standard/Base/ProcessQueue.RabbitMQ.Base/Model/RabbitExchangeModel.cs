﻿using ProcessQueue.RabbitMQ.Base.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace ProcessQueue.RabbitMQ.Base.Model
{
    public class RabbitExchangeModel : ConfigurationElement,  IRabbitExchangeModel
    {
        [ConfigurationProperty("Name", IsRequired = true)]
        public string Name
        {
            get => (string)this[ApplicationStrings.ConfigurationRabbitExchangeModelNamePropertyName];
            set
            {
                this[ApplicationStrings.ConfigurationRabbitExchangeModelNamePropertyName] = value;
            }
        }

        [ConfigurationProperty("Type", IsRequired = true)]
        public ExchangeType Type
        {
            get
            {
                return (ExchangeType)this[ApplicationStrings.ConfigurationRabbitExchangeModelTypePropertyName];
            }
            set
            {
                this[ApplicationStrings.ConfigurationRabbitExchangeModelTypePropertyName] = value;
            }
        }
    }
}
