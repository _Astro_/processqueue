﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProcessQueue.RabbitMQ.Base.Model
{
    public interface IRabbitConnectionModel
    {
        string UserName { get; set; }
        string Password { get; set; }
        string VirtualHost { get; set; }
        string HostName { get; set; }
        int Port { get; set; }
    }
}
