﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace ProcessQueue.RabbitMQ.Base.Model
{
    [ConfigurationCollection(typeof(RabbitRoutingKeyModel), AddItemName = "RabbitRoutingKey")]

    public class RabbitRoutingKeyCollection : ConfigurationElementCollection, IEnumerable<RabbitRoutingKeyModel>
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new RabbitRoutingKeyModel();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return (element as RabbitRoutingKeyModel).Name;
        }

        IEnumerator<RabbitRoutingKeyModel> IEnumerable<RabbitRoutingKeyModel>.GetEnumerator()
        {
            foreach (var key in this.BaseGetAllKeys())
            {
                yield return (RabbitRoutingKeyModel)BaseGet(key);
            }
        }
    }
}
