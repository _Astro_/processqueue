﻿using ProcessQueue.RabbitMQ.Base.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace ProcessQueue.RabbitMQ.Base.Model
{
    public class RabbitEnvironment : ConfigurationSection, IRabbitEnvironment
    {
        [ConfigurationProperty("RabbitConnection", IsRequired = true)]
        public RabbitConnectionModel RabbitConnection
        {
            get
            {
                return (RabbitConnectionModel)this[ApplicationStrings.ConfigurationRabbitConnectionModelSectionName];
            }
            set
            { 
                this[ApplicationStrings.ConfigurationRabbitConnectionModelSectionName] = value; 
            }
        }

        [ConfigurationProperty("RabbitCore", IsRequired = true)]
        public RabbitCoreModel RabbitCore
        {
            get
            {
                return (RabbitCoreModel)this[ApplicationStrings.ConfigurationRabbitCoreModelSectionName];
            }
            set
            {
                this[ApplicationStrings.ConfigurationRabbitCoreModelSectionName] = value;
            }
        }

        [ConfigurationProperty("RabbitQueues", IsDefaultCollection = true)]
        public RabbitQueueCollection RabbitQueues
        {
            get
            {
                return (RabbitQueueCollection)this[ApplicationStrings.ConfigurationRabbitQueueCollectionName];
            }
            set
            {
                this[ApplicationStrings.ConfigurationRabbitQueueCollectionName] = value;
            }
        }

        [ConfigurationProperty("RabbitExchanges", IsDefaultCollection = true)]
        public RabbitExchangeCollection RabbitExchanges
        {
            get
            {
                return (RabbitExchangeCollection)this[ApplicationStrings.ConfigurationRabbitExchangeCollectionName];
            }
            set
            {
                this[ApplicationStrings.ConfigurationRabbitExchangeCollectionName] = value;
            }
        }
    }
}
