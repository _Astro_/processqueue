﻿namespace ProcessQueue.RabbitMQ.Base.Model
{
    public interface IRabbitEnvironment
    {
        RabbitConnectionModel RabbitConnection { get; set; }
        RabbitCoreModel RabbitCore { get; set; }
        RabbitQueueCollection RabbitQueues { get; set; }
        RabbitExchangeCollection RabbitExchanges { get; set; }
    }
}