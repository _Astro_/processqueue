﻿using ProcessQueue.RabbitMQ.Base.Common;

namespace ProcessQueue.RabbitMQ.Base.Model
{
    public interface IRabbitExchangeModel
    {
        string Name { get; set; }
        ExchangeType Type { get; set; }
    }
}