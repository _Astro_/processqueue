﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace ProcessQueue.RabbitMQ.Base.Model
{
    [ConfigurationCollection(typeof(RabbitQueueModel), AddItemName = "RabbitQueue")]
    public class RabbitQueueCollection : ConfigurationElementCollection, IEnumerable<RabbitQueueModel>
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new RabbitQueueModel();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return (element as RabbitQueueModel).Lp;
        }

        IEnumerator<RabbitQueueModel> IEnumerable<RabbitQueueModel>.GetEnumerator()
        {
            foreach (var key in this.BaseGetAllKeys())
            {
                yield return (RabbitQueueModel)BaseGet(key);
            }
        }
    }
}
