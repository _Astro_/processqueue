﻿using ProcessQueue.RabbitMQ.Base.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace ProcessQueue.RabbitMQ.Base.Model
{
    public class RabbitCoreModel : ConfigurationElement,  IRabbitCoreModel
    {
        [ConfigurationProperty("Communication", IsRequired = true)]
        public ServiceCommunicationType Communication
        {
            get
            {
                return (ServiceCommunicationType)this[ApplicationStrings.ConfigurationRabbitCoreModelCommunicationName];
            }
            set
            { 
                this[ApplicationStrings.ConfigurationRabbitCoreModelCommunicationName] = value; 
            }
        }

        [ConfigurationProperty("Durable", IsRequired = true)]
        public bool Durable
        {
            get
            {
                return (bool)this[ApplicationStrings.ConfigurationRabbitCoreModelDurableName];
            }
            set
            { 
                this[ApplicationStrings.ConfigurationRabbitCoreModelDurableName] = value; 
            }
        }
    }
}
