﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProcessQueue.RabbitMQ.Base.Service
{
    /// <summary>
    /// Klasa przetwarzania odebranej wiadomości
    /// </summary>
    public interface IMessageProcessor
    {
        T ProcessMessage<T, Q>(Q messageData);
    }
}
