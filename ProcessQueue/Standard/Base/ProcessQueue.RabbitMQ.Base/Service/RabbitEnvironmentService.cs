﻿using ProcessQueue.RabbitMQ.Base.Common;
using ProcessQueue.RabbitMQ.Base.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace ProcessQueue.RabbitMQ.Base.Service
{
    /// <summary>
    /// Klasa usług parametryzacji z App.config
    /// </summary>
    public class RabbitEnvironmentService
    {
        /// <summary>
        /// Metoda zwraca klasę parametryzacji środowiska z App.config
        /// </summary>
        /// <returns></returns>
        public static IRabbitEnvironment GetRabbitEnvironmentFromConfig()
        {
            return (IRabbitEnvironment)ConfigurationManager.GetSection(ApplicationStrings.ConfigurationRabbitEnvironmentSectionName);
        }

        /// <summary>
        /// Metoda walidacji poprawności konfiguracji środowiska z App.config
        /// </summary>
        /// <param name="rabbitEnvironment">Klasa parametrów konfiguracji</param>
        /// <returns></returns>
        public static bool ValidateRabbitEnvironment(IRabbitEnvironment rabbitEnvironment)
        {
            switch (rabbitEnvironment.RabbitCore.Communication)
            {
                case ServiceCommunicationType.Queue:
                    return ValidateEnvironmentQueueBase(rabbitEnvironment);
                case ServiceCommunicationType.Exchange:
                    return ValidateEnvironmentExchangeBase(rabbitEnvironment);
            }

            return true;
        }

        /// <summary>
        /// Walidacja parametrów środowiska dla trybu komunikacji: Queue
        /// </summary>
        /// <param name="rabbitEnvironment">Klasa parametrów środowiska</param>
        /// <returns></returns>
        private static bool ValidateEnvironmentQueueBase(IRabbitEnvironment rabbitEnvironment)
        {
            if (rabbitEnvironment.RabbitQueues.ToList().Count == 0)
                throw new Exception(ApplicationStrings.ValidateQueueNoQueues);

            foreach (var queue in rabbitEnvironment.RabbitQueues.ToList())
            {
                if (string.IsNullOrEmpty(queue.Name))
                    throw new Exception(ApplicationStrings.ValidateQueueEmptyName);
            }

            return true;
        }

        /// <summary>
        /// Walidacja parametrów środowiska dla trybu komunikacji: Exchange
        /// </summary>
        /// <param name="rabbitEnvironment">Klasa parametrów środowiska</param>
        /// <returns></returns>
        private static bool ValidateEnvironmentExchangeBase(IRabbitEnvironment rabbitEnvironment)
        {
            if (rabbitEnvironment.RabbitExchanges.ToList().Count == 0)
                throw new Exception(ApplicationStrings.ValidateExchangeNoExchange);

            foreach (var exchange in rabbitEnvironment.RabbitExchanges.ToList())
            {
                if (exchange.Type != ExchangeType.direct)
                    throw new Exception(ApplicationStrings.ValidateExchangeTypeDifferentThanDirect);
            }

            foreach (var queue in rabbitEnvironment.RabbitQueues.ToList())
            {
                if (string.IsNullOrEmpty(queue.Exchange))
                    throw new Exception(ApplicationStrings.ValidateExchangeNoExchange);

                if (queue.RabbitRoutingKeys == null || queue.RabbitRoutingKeys.ToList().Count == 0)
                    throw new Exception(ApplicationStrings.ValidateQueueNoRoutingKeys);

                var anyExchange = (from exchange in rabbitEnvironment.RabbitExchanges.ToList()
                                   where exchange.Name == queue.Exchange
                                   select exchange).FirstOrDefault();

                if (anyExchange == null)
                    throw new Exception(ApplicationStrings.ValidateQueueNoExchangeRelation);
            }

            return true;
        }
    }
}
