﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProcessQueue.RabbitMQ.Base.Common
{
    /// <summary>
    /// Wyliczenie typów centrów
    /// </summary>
    public enum ExchangeType
    {
        fanout,
        direct,
        topic,
        headers
    }
}
