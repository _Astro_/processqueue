﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProcessQueue.RabbitMQ.Base.Common
{
    /// <summary>
    /// Wyliczenie typów komunikacji z serwisem RabbitMQ
    /// </summary>
    public enum ServiceCommunicationType
    {
        Queue,
        Exchange,
        Undefined
    }
}
