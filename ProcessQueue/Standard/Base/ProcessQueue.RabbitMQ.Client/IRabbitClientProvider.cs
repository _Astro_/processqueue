﻿using ProcessQueue.RabbitMQ.Base.Model;
using System.Threading;

namespace ProcessQueue.RabbitMQ.Client
{
    /// <summary>
    /// Interfejs klasy uruchomieniowej dla klienta RabbitMQ
    /// </summary>
    public interface IRabbitClientProvider
    {
        void StartWinServiceConsume(CancellationToken cancellationToken);
    }
}