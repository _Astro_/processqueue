﻿using ProcessQueue.RabbitMQ.Base.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProcessQueue.RabbitMQ.Client
{
    /// <summary>
    /// RabbitMQ interfejs klasy klienta
    /// </summary>
    public interface IRabitClient
    {
        Task ConsumeQueueContinuously(IRabbitQueueModel rabbitQueueModel, bool durable, CancellationToken cancellationToken);
        Task ConsumeExchangeQueueContinuously(IRabbitExchangeModel rabbitExchangeModel, IRabbitQueueModel rabbitQueueModel, bool durable, CancellationToken cancellationToken);
        void DestroyQueue(string queueName);
    }
}
