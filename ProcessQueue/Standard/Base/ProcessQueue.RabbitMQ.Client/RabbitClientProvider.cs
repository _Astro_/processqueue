﻿using ProcessQueue.RabbitMQ.Base.Model;
using ProcessQueue.RabbitMQ.Base.Service;
using ProcessQueue.RabbitMQ.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProcessQueue.RabbitMQ.Client
{
    /// <summary>
    /// Klasa uruchomieniowa dla klienta serwisu RabbitMQ
    /// </summary>
    public class RabbitClientProvider : IRabbitClientProvider
    {
        private IRabbitEnvironment _rabbitEnvironment;
        private IMessageProcessor _messageProcessor;
        private CancellationToken _cancellationToken;

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="rabbitEnvironment"></param>
        /// <param name="messageProcessor"></param>
        public RabbitClientProvider(IRabbitEnvironment rabbitEnvironment, IMessageProcessor messageProcessor)
        {
            _rabbitEnvironment = rabbitEnvironment;
            _messageProcessor = messageProcessor;
        }

        /// <summary>
        /// Uruchomienie ciągłe dla serwisu typu Windows Service
        /// </summary>
        public void StartWinServiceConsume(CancellationToken cancellationToken)
        {
            _cancellationToken = cancellationToken;

            switch (_rabbitEnvironment.RabbitCore.Communication)
            {
                case Base.Common.ServiceCommunicationType.Queue:
                    RunRabbitWithQueuesForWinService(cancellationToken);
                    break;
                case Base.Common.ServiceCommunicationType.Exchange:
                    RunRabbitWitchExchangesForWinService(cancellationToken);
                    break;
            }
        }

        /// <summary>
        /// Uruchomienie ciągłe kolejki dla WinService
        /// </summary>
        /// <param name="cancellationToken">Token anulowania</param>
        private async void RunRabbitWithQueuesForWinService(CancellationToken cancellationToken)
        {
            try
            {
                var rabbitClient = new RabbitClient(_messageProcessor, _rabbitEnvironment);

                foreach (var queue in _rabbitEnvironment.RabbitQueues.ToList())
                {
                    await Task.Run(() => rabbitClient.ConsumeQueueContinuously(queue, _rabbitEnvironment.RabbitCore.Durable, cancellationToken).ConfigureAwait(false));
                }
            }
            catch (Exception exception)
            {
                RabbitLogger.RegisterGeneralError(exception);
            }
        }

        /// <summary>
        /// Uruchomienie ciągłe centrum z kolejkami dla WinService
        /// </summary>
        /// <param name="cancellationToken">Token anulowania</param>
        private async void RunRabbitWitchExchangesForWinService(CancellationToken cancellationToken)
        {
            var rabbitClient = new RabbitClient(_messageProcessor, _rabbitEnvironment);

            try
            {
                foreach(var exchange in _rabbitEnvironment.RabbitExchanges.ToList())
                {
                    foreach(var queue in _rabbitEnvironment.RabbitQueues.ToList())
                    {
                        if (queue.Exchange == exchange.Name)
                            await Task.Run(() => rabbitClient.ConsumeExchangeQueueContinuously(exchange, queue, _rabbitEnvironment.RabbitCore.Durable, cancellationToken).ConfigureAwait(false));
                    }
                }
            }
            catch (Exception exception)
            {
                RabbitLogger.RegisterGeneralError(exception);
            }
        }
    }
}
