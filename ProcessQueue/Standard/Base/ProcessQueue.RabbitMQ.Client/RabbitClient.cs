﻿using ProcessQueue.RabbitMQ.Base.Connection;
using ProcessQueue.RabbitMQ.Base.Model;
using ProcessQueue.RabbitMQ.Base.Service;
using ProcessQueue.RabbitMQ.Logger;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProcessQueue.RabbitMQ.Client
{
    /// <summary>
    /// RabbitMQ klasa klienta
    /// </summary>
    public class RabbitClient : IRabitClient
    {
        private IMessageProcessor _messageProcessor;
        private IRabbitEnvironment _rabbitEnvironment;

        /// <summary>
        /// Ctor
        /// </summary>
        public RabbitClient(IMessageProcessor messageProcessor, IRabbitEnvironment rabbitEnvironment)
        {
            _messageProcessor = messageProcessor;
            _rabbitEnvironment = rabbitEnvironment;
        }

        /// <summary>
        /// Metoda odbioru danych z kolejki
        /// </summary>
        /// <param name="queueName">Nazwa kolejki</param>
        /// <param name="durable">Czy kolejka jest stała</param>
        /// <param name="cancellationToken">Token anulowania</param>
        /// <returns></returns>
        public async Task ConsumeQueueContinuously(IRabbitQueueModel rabbitQueueModel, bool durable, CancellationToken cancellationToken)
        {
            try
            {
                var connectionFactory = RabbitFactory.GetConnectionFactory(_rabbitEnvironment.RabbitConnection);
                using (var connection = RabbitFactory.GetConnection(connectionFactory))
                {
                    using (var channel = RabbitFactory.GetChannel(connection))
                    {
                        channel.QueueDeclare(queue: rabbitQueueModel.Name,
                                 durable: durable,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);

                        var consumer = new EventingBasicConsumer(channel);

                        string message = string.Empty;

                        consumer.Received += (model, ea) =>
                        {
                            try
                            {
                                var body = ea.Body.ToArray();
                                message = Encoding.UTF8.GetString(body);

                                var result =_messageProcessor.ProcessMessage<string, string>(message);
                                RabbitLogger.RegisterGeneralMessage(result);
                            }
                            catch (Exception exception)
                            {
                                RabbitLogger.RegisterGeneralError(exception);
                            }
                        };

                        channel.BasicConsume(queue: rabbitQueueModel.Name,
                                             autoAck: true,
                                             consumer: consumer);

                        // Dla podtrzymania działania
                        while (true)
                        {
                            if (cancellationToken.IsCancellationRequested)
                                return;

                            await Task.Delay(50);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                RabbitLogger.RegisterGeneralError(exception);
            }
        }

        /// <summary>
        /// Metoda odbioru danych z kolejki centrum
        /// </summary>
        /// <param name="rabbitExchangeModel">Klasa parametrów centrum</param>
        /// <param name="rabbitQueueModel">Kolejka podpinana do centrum</param>
        /// <param name="durable">Czy obiekty stałe/tymczasowe</param>
        /// <param name="cancellationToken">Token anulowania</param>
        /// <returns></returns>
        public async Task ConsumeExchangeQueueContinuously(IRabbitExchangeModel rabbitExchangeModel, IRabbitQueueModel rabbitQueueModel, bool durable, CancellationToken cancellationToken)
        {
            try
            {
                var connectionFactory = RabbitFactory.GetConnectionFactory(_rabbitEnvironment.RabbitConnection);
                using (var connection = RabbitFactory.GetConnection(connectionFactory))
                {
                    using (var channel = RabbitFactory.GetChannel(connection))
                    {
                        // Deklaracja centrum
                        channel.ExchangeDeclare(exchange: rabbitExchangeModel.Name,
                            type: rabbitExchangeModel.Type.ToString(),
                            durable: durable,
                            autoDelete: false,
                            arguments: null
                            );

                        string queueName;

                        // Kolejka stała lub tymczasowa
                        if (durable)
                        {
                            queueName = rabbitQueueModel.Name;

                            // Deklaracja kolejki
                            channel.QueueDeclare(queue: queueName,
                                                    durable: durable,
                                                    exclusive: false,
                                                    autoDelete: false,
                                                    arguments: null);
                        }
                        else
                        {
                            queueName = channel.QueueDeclare().QueueName;
                        }

                        foreach (var routingKey in rabbitQueueModel.RabbitRoutingKeys.ToList())
                        {
                            // Powiązanie centrum i kolejki z kluczem routingu
                            channel.QueueBind(queue: queueName,
                                                exchange: rabbitQueueModel.Exchange,
                                                routingKey: routingKey.Name,
                                                arguments: null
                                                );
                        }

                        var consumer = new EventingBasicConsumer(channel);

                        string message = string.Empty;

                        consumer.Received += (model, ea) =>
                        {
                            try
                            {
                                var body = ea.Body.ToArray();
                                message = Encoding.UTF8.GetString(body);

                                var result = _messageProcessor.ProcessMessage<string, string>(message);
                                RabbitLogger.RegisterGeneralMessage(result);
                            }
                            catch (Exception exception)
                            {
                                RabbitLogger.RegisterGeneralError(exception);
                            }
                        };

                        channel.BasicConsume(queue: queueName,
                                             autoAck: true,
                                             consumer: consumer);

                        // Dla podtrzymania działania
                        while (true)
                        {
                            if (cancellationToken.IsCancellationRequested)
                                return;

                            await Task.Delay(50);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                RabbitLogger.RegisterGeneralError(exception);
            }
        }

        /// <summary>
        /// Metoda usuwa kolejkę z serwisu RabbitMQ
        /// </summary>
        /// <param name="queueName">Kolejka do usunięcia</param>
        public void DestroyQueue(string queueName)
        {
            var connectionFactory = RabbitFactory.GetConnectionFactory(_rabbitEnvironment.RabbitConnection);
            var connection = RabbitFactory.GetConnection(connectionFactory);
            var channel = RabbitFactory.GetChannel(connection);
            channel.QueueDelete(queueName);

            connection.Close();
            connection.Dispose();
        }
    }
}