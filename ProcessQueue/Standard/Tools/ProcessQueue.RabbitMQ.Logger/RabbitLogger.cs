﻿using System;
using System.IO;

namespace ProcessQueue.RabbitMQ.Logger
{
    /// <summary>
    /// Klasa logowania błędów i wyników
    /// </summary>
    public static class RabbitLogger
    {
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Logowanie błędu ogólnego
        /// </summary>
        /// <param name="exception">Błąd do zalogowania</param>
        public static void RegisterGeneralError(Exception exception)
        {
            _logger.Error(exception);
        }

        /// <summary>
        /// Logowanie komunikatu
        /// </summary>
        /// <param name="message">Komunikat do zalogowania</param>
        public static void RegisterGeneralMessage(string message)
        {
            _logger.Info(message);
        }
    }
}
