using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ProcessQueue.RabbitMQ.Base.Model;
using ProcessQueue.RabbitMQ.Base.Service;
using ProcessQueue.RabbitMQ.Client;
using ProcessQueue.RabbitMQ.Logger;

namespace ProcessQueue.LinuxService
{
    /// <summary>
    /// Serwis consumera kolejki : wersja Linux .Net Core na podstawie klasy BackgroundService 
    /// </summary>
    public class ProcessQueueWorker : BackgroundService
    {
        /// <summary>
        /// Metoda uruchomieniowa serwisu
        /// </summary>
        /// <param name="cancellationToken">Token anulowania</param>
        /// <returns></returns>
        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            await Task.Run(() => RunWorker(cancellationToken).ConfigureAwait(false));
        }

        /// <summary>
        /// Uruchomienie obs�ugi rabbita
        /// </summary>
        /// <param name="cancellationToken">Token anulowania</param>
        /// <returns></returns>
        private async Task RunWorker(CancellationToken cancellationToken)
        {
            try
            {
                var rabbitEnvironment = RabbitEnvironmentService.GetRabbitEnvironmentFromConfig();
                RabbitEnvironmentService.ValidateRabbitEnvironment(rabbitEnvironment);
                var rabbitClientProvider = GetNewRabbitClientProviderInstance(rabbitEnvironment, null);
                rabbitClientProvider.StartWinServiceConsume(cancellationToken);
            }
            catch (Exception exception)
            {
                LogError(exception);
            }

            // Dla podtrzymania dzia�ania
            while (true)
            {
                if (cancellationToken.IsCancellationRequested)
                    return;

                await Task.Delay(50);
            }
        }

        /// <summary>
        /// Metoda logowania b��d�w
        /// </summary>
        /// <param name="exception">B��d do zalogowania</param>
        private void LogError(Exception exception)
        {
            RabbitLogger.RegisterGeneralError(exception);
        }

        /// <summary>
        /// Metoda zwraca now� instancj� klasy RabbitClientProvider
        /// </summary>
        /// <param name="rabbitEnvironment">Klasa parametryzacji �rodowiska</param>
        /// <param name="messageProcessor">Klasa przetwarzania wiadomo�ci</param>
        /// <returns></returns>
        private IRabbitClientProvider GetNewRabbitClientProviderInstance(IRabbitEnvironment rabbitEnvironment, IMessageProcessor messageProcessor)
        {
            return new RabbitClientProvider(rabbitEnvironment, messageProcessor);
        }
    }
}
