﻿using ProcessQueue.RabbitMQ.Base.Model;
using ProcessQueue.RabbitMQ.Base.Service;
using ProcessQueue.RabbitMQ.Client;
using ProcessQueue.RabbitMQ.Logger;
using System;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;

namespace ProcessQueue.WinService
{
    /// <summary>
    /// Serwis consumera kolejki : wersja .Net Core
    /// </summary>
    public class ProcessQueueCoreService : ServiceBase
    {
        private int _eventId = 0;
        private ManualResetEvent _shutdownEvent = new ManualResetEvent(false);
        private Thread _thread;
        private EventLog _serviceEventLog;
        private CancellationTokenSource _cancellationTokenSource;
        private CancellationToken _cancellationToken;

        /// <summary>
        /// Ctor
        /// </summary>
        public ProcessQueueCoreService()
        {
            if (!EventLog.SourceExists("PrcQueCorSrvSource"))
                EventLog.CreateEventSource("PrcQueCorSrvSource", "PrcQueCorSrvLog");

            _serviceEventLog = new EventLog();
            _serviceEventLog.Source = "PrcQueCorSrvSource";
            _serviceEventLog.Log = "PrcQueCorSrvLog";
        }

        /// <summary>
        /// Metoda uruchomienia serwisu w wątku
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {
            StartThreadService();
        }

        /// <summary>
        /// Metoda uruchamia proces na wątku
        /// </summary>
        private void StartThreadService()
        {
            _cancellationTokenSource = new CancellationTokenSource();
            _cancellationToken = _cancellationTokenSource.Token;
            _thread = new Thread(WorkerThreadFunc);
            _thread.Name = "PrcQueCorSrvWorker";
            _thread.IsBackground = true;
            _thread.Start();
        }

        /// <summary>
        /// Metoda właściwa uruchomienia serwisu
        /// </summary>
        private void WorkerThreadFunc()
        {
            _serviceEventLog.WriteEntry("Process Queue Core Service Start", EventLogEntryType.Information, _eventId++);
            RunService();
        }

        /// <summary>
        /// Metoda obsługi wyłączenia serwisu
        /// </summary>
        protected override void OnStop()
        {
            StopThreadService();
            _serviceEventLog.WriteEntry("Process Queue Core Service Stop", EventLogEntryType.Information, _eventId);
        }

        /// <summary>
        /// Metoda zatrzymuje wątek serwisu, na którym uruchomiony jest proces
        /// </summary>
        private void StopThreadService()
        {
            _cancellationTokenSource.Cancel();
            _shutdownEvent.Set();

            if (!_thread.Join(3000)) // 3 sekundy na zatrzymanie
            {
                _cancellationTokenSource.Dispose();
                _thread.Abort();
            }
        }

        /// <summary>
        /// Metoda wywołania logiki usługi
        /// </summary>
        private void RunService()
        {
            try
            {
                var rabbitEnvironment = RabbitEnvironmentService.GetRabbitEnvironmentFromConfig();
                RabbitEnvironmentService.ValidateRabbitEnvironment(rabbitEnvironment);
                var rabbitClientProvider = GetNewRabbitClientProviderInstance(rabbitEnvironment, null);
                rabbitClientProvider.StartWinServiceConsume(_cancellationToken);
            }
            catch (Exception exception)
            {
                LogError(exception);
            }
        }

        /// <summary>
        /// Metoda logowania błędów
        /// </summary>
        /// <param name="exception">Błąd do zalogowania</param>
        private void LogError(Exception exception)
        {
            RabbitLogger.RegisterGeneralError(exception);
        }

        /// <summary>
        /// Metoda zwraca nową instancję klasy RabbitClientProvider
        /// </summary>
        /// <param name="rabbitEnvironment">Klasa parametryzacji środowiska</param>
        /// <param name="messageProcessor">Klasa przetwarzania wiadomości</param>
        /// <returns></returns>
        private IRabbitClientProvider GetNewRabbitClientProviderInstance(IRabbitEnvironment rabbitEnvironment, IMessageProcessor messageProcessor)
        {
            return new RabbitClientProvider(rabbitEnvironment, messageProcessor);
        }
    }
}