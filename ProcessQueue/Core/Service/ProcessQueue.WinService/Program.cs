﻿using System;
using System.ServiceProcess;

namespace ProcessQueue.WinService
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceBase.Run(new ProcessQueueCoreService());
        }
    }
}
